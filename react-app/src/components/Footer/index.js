import React, { Component } from 'react';
import { withRouter } from 'react-router';

class Footer extends Component {

    render() {
        return (
            <div class="section is-small has-background-light">
                <footer class="footer">
                    <div class="container">
                        <div class="columns is-vcentered">
                            <div class="column is-half has-text-centered-mobile">
                                <div class="content">
                                    <p>Copyright &copy; 2019 <b>B2e</b>volve. <span class="is-hidden-tablet"><br /></span> All rights reserved.<br />
                                        <a href="https://b2evolve.com/privacy-policy.html" class="is-size-7 is-size-6-mobile has-text-grey is-footer-link">Privacy Policy</a>
                                        <a href="https://b2evolve.com/contact-us.html" class="is-size-7 is-size-6-mobile has-text-grey is-footer-link">Contact</a>
                                    </p>
                                </div>
                            </div>
                            <div class="column is-half has-text-right-tablet has-text-centered-mobile social">
                                <span class="icon is-size-4">
                                    <a href="https://www.facebook.com/b2evolve/" target="_blank">
                                        <i class="icon icon-facebook" title="facebook"></i>
                                    </a>
                                </span>
                                <span class="icon is-size-4">
                                    <a href="https://www.linkedin.com/company/b2evolve/" target="_blank">
                                        <i class="icon icon-linkedin" title="LinkedIn"></i>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}

export default withRouter(Footer); 
