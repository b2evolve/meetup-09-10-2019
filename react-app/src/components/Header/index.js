import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { withRouter } from 'react-router';
//import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
//import './Header.scss';
//require('../Utils/Icons');

class Header extends Component {

    /*get homeLink() {
        let currLocation;
        currLocation = this.props.location.pathname;
        currLocation = currLocation.substr(0, currLocation.length - 5);

        if (this.props.navigationRoot && currLocation !== this.props.navigationRoot) {
            return (<Link className="Header-action" to={this.props.navigationRoot + ".html"}>
                <FontAwesomeIcon icon="chevron-left" />
            </Link>);
        }
        return null;
    }*/

    render() {
        return (
            <nav class="navbar has-shadow" role="navigation" aria-label="main navigation" id="start">
                <div class="container">
                    <div class="navbar-brand">
                    <a class="navbar-item" href="/">
                        <img src="https://b2evolve.com/assets/img/logo.svg" width="150" height="28" alt="B2evolve"/>
                    </a>
                    <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    </a>
                    </div>
                    <div id="navbarBasicExample" class="navbar-menu">
                    <div class="navbar-end">
                    <div class="navbar-item has-dropdown is-hoverable">
                    <a href="https://b2evolve.com/who-we-are.html" class="navbar-link is-arrowless">Who We Are</a>
                    </div>
                    <div class="navbar-item has-dropdown is-hoverable">
                    <a href="https://b2evolve.com/what-we-do.html" class="navbar-link is-arrowless">What We Do</a>
                    <div class="navbar-dropdown">
                    <a href="https://b2evolve.com/digital-strategy.html" class="navbar-item">Digital Strategy</a>
                    <a href="https://b2evolve.com/technologies.html" class="navbar-item">Technologies</a>
                    <a href="https://b2evolve.com/services.html" class="navbar-item">Services</a>
                    </div>
                    </div>
                    <div class="navbar-item has-dropdown is-hoverable">
                    <a href="/content/mysamplespa/en/blog.html" class="navbar-link is-arrowless">Blog</a>
                    </div>
                    </div>
                    </div>
                </div>
            </nav>
        );
    }
}

export default withRouter(Header); 
