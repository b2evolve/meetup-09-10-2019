import React, {Component} from 'react';
import {MapTo} from '@adobe/cq-react-editable-components';
import {Link} from "react-router-dom";
import { getImage } from '../Utils';
//require('./List.scss');

const ListEditConfig = {

    emptyLabel: 'List',

    isEmpty: function(props) {
        return !props || !props.items || props.items.length < 1;
    }
};
export function convertDate (date) {
    let dateFormatted = new Date(date);
    return dateFormatted.toLocaleDateString('en-US');
};

/**
 * ListItem renders the individual items in the list
 */
class ListItem extends Component {

    get date() {
       if(!this.props.date) {
           return null;
       }
        let date = new Date(this.props.date);
        return date.toLocaleDateString('en-US');
    }

    render() {
        if(!this.props.path || !this.props.title || !this.props.url) {
            return null;
        }
        return (
            <li className="ListItem">
                <Link className="ListItem-link" to={this.props.url}>{this.props.title}
                    <span className="ListItem-date">{this.date}</span>
                </Link>
            </li>
        );
    }
}
export default class List extends Component {
    render() {
        return(
            <section class="section ">
            <div class="container">
                <div class="columns is-multiline">
                        { this.props.items && this.props.items.map((listItem, index) => {
                            return <div class="column is-half-tablet is-one-third-desktop">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="content">
                                            <p>{listItem.title} </p>
                                            <time>{convertDate(listItem.lastModified)}</time>
                                            <a class="button is-text is-pulled-right" href={listItem.url}>Read</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            })
                       }

                   
                </div>
            </div>
        </section>

        );
    }
}

MapTo("mysamplespa/components/list")(List, ListEditConfig);