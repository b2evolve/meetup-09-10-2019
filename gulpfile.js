//gulpfile.js
const gulp = require('gulp');
const sass = require('gulp-sass');

//style paths
const sassFiles = 'ui.apps/src/main/content/jcr_root/apps/aemfed/**/*.scss',
    cssDest = 'ui.apps/src/main/content/jcr_root/apps/aemfed/';

gulp.task('styles', function () {
    return gulp.src(sassFiles)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(cssDest));
});

gulp.task('watch', function () {
    return gulp.watch(sassFiles, gulp.series('styles'));
});