# AEM Meetup 9/10/2019

This is a content package for an AEM project set up with the SPA Maven Archetype for AEM (aem-spa-project-archetype).

### UberJar
 
This project relies on the unobfuscated AEM 6.4 cq-quickstart. This is publicly available on https://repo.adobe.com
 
For more details about the UberJar please head over to the
[How to Build AEM Projects using Apache Maven](https://helpx.adobe.com/experience-manager/6-4/sites/developing/using/ht-projects-maven.html)
documentation page.
 
### Install everything
 
In the All-in-One package in [all](all/) there is additional profile available to package all the dependencies into a 
single content package and install everything to an existing AEM
 
 * ``autoInstallSinglePackage`` - installs the All-in-One package to an existing AEM author instance
 * ``autoInstallSinglePackagePublish`` - installs the All-in-One package to an existing AEM publish instance

 
    mvn clean install -PautoInstallSinglePackage

You can also choose build environment by using setting `build.environment` property (format: colon + name):

    mvn clean install -PautoInstallSinglePackage -Dbuild.environment=":production"

## System requirements

* JDK 1.8 or higher
* Apache Maven 3.5.0 or higher
* Include the [Adobe Public Maven Repository][adobe-public-maven-repo] in your maven settings

It is recommended to set up the local AEM instances with `nosamplecontent` run mode.

## Maven settings

The project comes with the auto-public repository configured. To setup the repository in your Maven settings, refer to:

    http://helpx.adobe.com/experience-manager/kb/SetUpTheAdobeMavenRepository.html
   