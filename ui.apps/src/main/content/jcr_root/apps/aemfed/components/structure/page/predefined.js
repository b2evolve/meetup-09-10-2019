use(function () {
    const pageName = currentPage.name;
    const title = currentPage.properties.get("jcr:title");
    const resourceName = granite.resource.name;
    const resourceTitle = properties.get("jcr:title");

    return {
        pageName: pageName,
        title: title,
        resourceName: resourceName,
        resourceTitle: resourceTitle,
        dateNow: new Date(),
    };
});