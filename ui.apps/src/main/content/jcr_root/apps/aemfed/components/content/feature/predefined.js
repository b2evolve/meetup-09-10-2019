use(function () {
    const resourceTitle = properties.get("jcr:title");
    const componentTitle = properties.get("title");
    const componentDescription = properties.get("description");

    return {
        title: componentTitle,
        description: componentDescription,
        resourceTitle: resourceTitle,
    };
});